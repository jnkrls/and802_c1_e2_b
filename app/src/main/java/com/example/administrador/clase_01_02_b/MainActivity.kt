package com.example.administrador.clase_01_02_b

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    override fun onResume() {
        super.onResume()

        btn_llamar1.setOnClickListener{
            //Cargamos un activity en especifico, si el permiso existe
            val intent = Intent()

            intent.action = "com.example.administrador.permiso.Aplicacion"
            intent.addCategory("android.intent.category.DEFAULT")

            try{
                startActivityForResult(intent,123)
            }catch (e:Exception){
                Toast.makeText(this, "No existe permiso", Toast.LENGTH_SHORT).show()
            }
        }

        btn_llamar2.setOnClickListener{
            //Obtenemos la ruta de la aplicacion
            val intent = packageManager.getLaunchIntentForPackage("com.example.administrador.clase01_02")
            startActivity(intent)

        }
    }
}
